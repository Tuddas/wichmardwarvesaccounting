import pandas as pd
import datetime

def dateparse(dt):
    return datetime.datetime.strptime(dt, '%d.%m.%Y')

def readCSVAbrechnung(filenameAbrechnung):
    headers = ['Buchungstag', 'Ausfuehrungstag', 'Textschluessel', 'Primanota', 'Zahlungsempfaenger', 'Zahlungsempfaengerkonto', 'ZahlungsempfaengerIBAN', 'ZahlungsempfaengerBLZ', 'ZahlungsempfaengerBIC', 'Verwendungszweck','Kundenreferenz', 'Waehrung', 'Umsatz', 'SollHaben']
    dtype = {'Buchungstag':'str', 'Ausfuehrungstag':'str', 'Textschluessel':'str', 'Primanota':'str', 'Zahlungsempfaenger':'str', 'Zahlungsempfaengerkonto':'str', 'ZahlungsempfaengerIBAN':'str', 'ZahlungsempfaengerBLZ':'str', 'ZahlungsempfaengerBIC':'str', 'Verwendungszweck':'str', 'Kundenreferenz':'str', 'Waehrung':'str', 'Umsatz':'float', 'SollHaben':'str'}
    parsedates = ['Buchungstag', 'Ausfuehrungstag']
    abrechnung = pd.read_csv(filenameAbrechnung, sep=';', skiprows=1, names=headers, decimal=',', dtype=dtype, parse_dates=parsedates, date_parser=dateparse)
    return abrechnung

def readCSVAbrechnungEdited(filenameAbrechnung):

    dtype = {'Buchungstag':'str',  'Zahlungsempfaenger':'str', 'Verwendungszweck':'str', 'Communitymember':'str', 'Umsatz':'float', 'Kategorie  ':'str'}
    parsedates = ['Buchungstag']
    abrechnung = pd.read_csv(filenameAbrechnung, sep=',', decimal=',', dtype=dtype, parse_dates=parsedates)
    return abrechnung