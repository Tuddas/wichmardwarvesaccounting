import pandas as pd
import datetime

categories = [                                     # list with all names of communitymembers
    "Miete",
    "Commons",
    "Essen",
    "GEZ",
    "Strom",
    "Kaffee",
    "Bode",
    "Anschaffungen",
    "Wechange",
    "Wlan",
    "Holz",
    "Bode",
    "Gruenfutter",
    "Voigt",
    "Verbrauchsgueter",
]

def cleanData(table, namesmapping, communitymember):
    # merge together SollHaben and Umsatz column
    tableSoll = table["SollHaben"] == "S"
    table.Umsatz[tableSoll] = -table.Umsatz[tableSoll]

    # delete obsolete columns
    table.drop(["Ausfuehrungstag", "Textschluessel", "Primanota",'Zahlungsempfaengerkonto', 'ZahlungsempfaengerIBAN', 'ZahlungsempfaengerBLZ', "ZahlungsempfaengerBIC", 'Kundenreferenz', 'Waehrung', 'SollHaben'], axis=1, inplace=True)

    # transfers made by communitymember: replace the entry "Zahlungsempfänger" with their unique community name
    table.Zahlungsempfaenger.replace(namesmapping, inplace=True)

    # insert a new column which shows if the transfer was made by a communitymember
    table.insert(3, "Communitymember", 0)
    table.Communitymember = table.Zahlungsempfaenger.isin(communitymember)

    table.sort_values(by='Buchungstag', inplace=True, ascending=True)
    return table

def addCategory(table):
    table.insert(5, "Kategorie", '')

    for i in range(table.shape[0]):
        if table["Verwendungszweck"].iloc[i].find("Miete") >= 0:
            table["Kategorie"].iloc[i] += "Miete"
        if table["Verwendungszweck"].iloc[i].find("Commons") >= 0:
            table["Kategorie"].iloc[i] += "Commons"
        if table["Verwendungszweck"].iloc[i].find("Puffer") >= 0:
            table["Kategorie"].iloc[i] += "Commons"
        if table["Verwendungszweck"].iloc[i].find("Spende") >= 0:
            table["Kategorie"].iloc[i] += "Commons"
        if table["Verwendungszweck"].iloc[i].find("Essen") >= 0:
            table["Kategorie"].iloc[i] += "Essen"
        if table["Verwendungszweck"].iloc[i].find("GEZ") >= 0:
            table["Kategorie"].iloc[i] += "GEZ"
        if table["Verwendungszweck"].iloc[i].find("Strom") >= 0:
           table["Kategorie"].iloc[i] += "Strom"
    return table

def checkConsistency(table):
    if any(~table.Kategorie.isin(categories)):
        print("WARNING: The following categories in the sheet are not in the categories list:")
        print(table.Kategorie[~table.Kategorie.isin(categories)])

    print("Please check if the the bank account currently equals: ")
    #print(table.Umsatz[table["Buchungstag"] < datetime.datetime(2022, 1, 1)].sum())
    print(table.Umsatz[table["Buchungstag"] <= table.Buchungstag.iloc[-1]].sum())
