
1. Install a python IDE (i.e. PyCharm  https://www.jetbrains.com/de-de/pycharm/download )
2. Install Python. I recommend Anaconda https://www.anaconda.com
3. Install the python packgages pandas and fpdf (for anaconda: "conda install package-name")

4. Import the project into your IDE and open the file WichmarDwarvesAccounting.py
5. Scroll to the "PARAMETERS" section and specify:
	- path to the accounting file
	- the name of the cleaned output file to be generated
	- update the list of communitymembers (make sure they are unique and no member is listed twice!)
	- update the namesmapping (mapping from the entries in "Zahlungsempfaenger to the list of communitymember.
						Please make sure every transfer of a community member in the accounting file
						is mapped to a unique name in the "communitymember"-list!)
6. Run WichmarDwarvesAccounting.py
7. Open the output file you specified before running WichmarDwarvesAccounting and add categories and balances of the different accounts
8. Open WichmarDwarvesAccounting2.py
9. Scroll to the "PARAMETERS" section and specify:
	- path to the cleaned accounting file from step 7
	- the amount of money on the account before the start of the accounting file (0.0 if new account without money on it)
	- carefully read possible warnings as a contingency check is run
	- carefully check if the bank account holds the amount of money displayed (amount of money befor + account movings
		from the file should equal the amount of money on the account after the time the file ends! if not, there may be errors!)
	- do some statistics if you like! :)