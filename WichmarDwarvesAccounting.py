import InputOutput as io
import DataProcessing as dp
import pandas as pd
from locale import setlocale, LC_NUMERIC

setlocale(LC_NUMERIC, 'German_Germany.1252')

############################ PARAMETERS ###################################################
pathToAccountingFile = 'Abrechnungen/21komplett.csv'    # path to accounting file
outputFile = 'Abrechnungen/Abrechnung2021.csv'
communitymember = [                                     # list with all names of communitymembers
    "TessRobert",
    "Rasmus",
    "Jakob",
    "MoritzNatalie",
    "KonnaJohanna",
    "PabloTabea",
    "CasparDiana",
    "Nele",
    "Krischan",
    "Anja",
    "Pierre",
    "Anna",
    "Markus",
    "Melina",
    "Susi",
    "Freddi"
]
namesmapping = {                                        # mapping from names in accounting file to unique name of communitymember
    "ROBERT HEUER UND THERESA HE UER": "TessRobert",    # please make sure every transfer of a community member in the accounting file
    "Theresa Heuer": "TessRobert",                      # is mapped to a unique name in the "communitymember"-list above
    "Kalle Rasmus Bartelt": "Rasmus",                   # check with print(abrechnung.Zahlungsempfaenger[abrechnung["Communitymember"] == 0])
    "Jakob Hafemann": "Jakob",
    "Hafemann, Jakob": "Jakob",
    "Moritz Leiner": "MoritzNatalie",
    "NATALIE HILMERS": "MoritzNatalie",
    "Konstantin Hoffmann": "KonnaJohanna",
    "KONSTANTIN HOFFMANN": "KonnaJohanna",
    "Johanna Rettner": "KonnaJohanna",
    "JOHANNA RETTNER": "KonnaJohanna",
    "Tabea See�elberg u Pablo Kr�mer": "PabloTabea",
    "Pablo Kr�mer": "PabloTabea",
    "Tabea See�elberg": "PabloTabea",
    "Caspar Lauterwasser": "CasparDiana",
    "DR. HELMUT LAUTERWASSER": "CasparDiana",
    "Helmut Lauterwasser": "CasparDiana",
    "Diana Spurite": "CasparDiana",
    "Kathrin Ambrosius": "Nele",
    "Krischan Lensch": "Krischan",
    "Lensch, Krischan": "Krischan",
    "Anja Monstadt": "Anja",
    "ANJA MONSTADT": "Anja",
    "Pierre Huber": "Pierre",
    "PIERRE HUBER": "Pierre",
    "Anna Lender": "Anna",
    "Markus Pflock": "Markus",
    "Melina Dietl": "Melina",
    "SUSANNE MARGARETE CHRISTA MARTIN": "Susi",
    "FREDERIK GOSAU": "Freddi"
}
###############################################################################################

abrechnung = io.readCSVAbrechnung(pathToAccountingFile)     # read accounting csv file
abrechnung = dp.cleanData(abrechnung, namesmapping, communitymember)    # clean up the table and prepare for evaluation
abrechnung = dp.addCategory(abrechnung)

pd.set_option('display.max_rows', 300)

abrechnung.to_csv(outputFile, index=False)

######################################## add additional output ##################################
#print(abrechnung)
#print(abrechnung.Zahlungsempfaenger[abrechnung["Communitymember"] == 0])


