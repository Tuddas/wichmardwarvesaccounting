import InputOutput as io
import DataProcessing as dp
import pandas as pd
import datetime
from locale import setlocale, LC_NUMERIC

setlocale(LC_NUMERIC, 'German_Germany.1252')

############################ PARAMETERS ###################################################
pathToAccountingFile = 'Abrechnungen/Abrechnung2021_edited.csv'    # path to accounting file
accountStatusBefore = 0.0                                          # money on bank account before accounting file starts
###############################################################################################

abrechnung = io.readCSVAbrechnungEdited(pathToAccountingFile)     # read accounting csv file
dp.checkConsistency(abrechnung)

######################################## add additional output ##################################
#print(abrechnung)


